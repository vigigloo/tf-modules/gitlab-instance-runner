resource "scaleway_instance_security_group" "runner" {
  inbound_default_policy  = "drop"
  outbound_default_policy = "accept"
  name                    = var.instance_prefix
}

data "gitlab_group" "group" {
  for_each = toset([for v in var.gitlab_groups : tostring(v)])
  group_id = tonumber(each.value)
}
resource "scaleway_instance_server" "group-runner" {
  for_each = toset([for v in var.gitlab_groups : tostring(v)])

  type  = var.scaleway_instance_type
  image = "ubuntu_focal"

  name = "${var.instance_prefix}-group-${data.gitlab_group.group[each.value].id}"
  tags = var.scaleway_instance_tags

  enable_dynamic_ip = var.enable_ipv4
  enable_ipv6       = var.enable_ipv6

  user_data = {
    cloud-init = templatefile("${path.module}/cloudinit.yml", {
      token          = data.gitlab_group.group[each.value].runners_token,
      gitlab_url     = var.gitlab_url,
      privileged     = var.privileged,
      tags           = var.gitlab_tags,
      default-image  = var.gitlab_default-image,
      run-untagged   = var.gitlab_run-untagged,
      timezone       = var.instance_timezone,
      runner-version = var.gitlab_runner-version,
    })
  }
  security_group_id = scaleway_instance_security_group.runner.id
}

data "gitlab_project" "project" {
  for_each = toset([for v in var.gitlab_projects : tostring(v)])
  id       = tonumber(each.value)
}
resource "scaleway_instance_server" "project-runner" {
  for_each = toset([for v in var.gitlab_projects : tostring(v)])

  type  = var.scaleway_instance_type
  image = "ubuntu_focal"

  name = "${var.instance_prefix}-project-${data.gitlab_project.project[each.value].id}"
  tags = var.scaleway_instance_tags

  enable_dynamic_ip = var.enable_ipv4
  enable_ipv6       = var.enable_ipv6

  user_data = {
    cloud-init = templatefile("${path.module}/cloudinit.yml", {
      token          = data.gitlab_project.project[each.value].runners_token,
      gitlab_url     = var.gitlab_url,
      privileged     = var.privileged,
      tags           = var.gitlab_tags,
      default-image  = var.gitlab_default-image,
      run-untagged   = var.gitlab_run-untagged,
      timezone       = var.instance_timezone,
      runner-version = var.gitlab_runner-version,
    })
  }
  security_group_id = scaleway_instance_security_group.runner.id
}
