variable "gitlab_groups" {
  type    = list(number)
  default = []
}

variable "gitlab_projects" {
  type    = list(number)
  default = []
}

variable "gitlab_url" {
  type    = string
  default = "https://gitlab.com/"
}

variable "privileged" {
  type    = bool
  default = false
}

variable "scaleway_instance_type" {
  type    = string
  default = "DEV1-S"
}

variable "instance_prefix" {
  type    = string
  default = "gitlab-runner"
}

variable "scaleway_instance_tags" {
  type    = list(string)
  default = ["gitlab runner"]
}

variable "gitlab_tags" {
  type    = list(string)
  default = ["with-docker", "scaleway", "alpine"]
}

variable "gitlab_default-image" {
  type    = string
  default = "alpine:latest"
}

variable "gitlab_run-untagged" {
  type    = bool
  default = false
}

variable "gitlab_runner-version" {
  type    = string
  default = "15.0.0"
}

variable "instance_timezone" {
  type    = string
  default = "UTC"
}

variable "enable_ipv4" {
  type    = bool
  default = false
}
variable "enable_ipv6" {
  type    = bool
  default = false
}
