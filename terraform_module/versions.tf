terraform {
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
    }
    scaleway = {
      source = "scaleway/scaleway"
    }
  }
  required_version = ">= 0.14"
}
